import express from "express"
import socketio from "socket.io"
import http from "http"
import uniqid from "uniqid"
const app = express()
const server = http.Server(app)
const io = socketio(server)
const port = 3000

app.use(express.static(__dirname))
server.listen(port, () => {
  console.log(`Listening on port ${port}!!`)
})

const defaultCommodities = {
  1: "Wheat",
  2: "Bananna",
  3: "Carrot",
  4: "Cherries",
  5: "Corn",
  6: "Grapes",
  7: "Green Pear",
  8: "Red Apple",
  9: "Watermelon"
}

let playerData = {}
let openTrades = {}
let otherData = {
  gameStarted: false
}

function getConnectedPlayers() {
  let connectedPlayers = []
  Object.keys(playerData).forEach(name => {
    if (playerData[name].connected){
      connectedPlayers.push({name, wins: playerData[name].wins})
    }
  })
  return connectedPlayers
}

function shuffleArray (array){
  for (let i = array.length - 1; i > 0; i--) {
    const rand = Math.floor(Math.random() * (i + 1));
    [array[i], array[rand]] = [array[rand], array[i]];
  }
}


function updateGameDataForAllPlayers() {
  const connectedPlayers = getConnectedPlayers()
  connectedPlayers.forEach(player => {
    const playerName = player.name
    let filteredTrades = []
    let myOffer = null
    Object.keys(openTrades).forEach(offerId => {
      const offer = openTrades[offerId]
      if (offer.seller === playerName) { myOffer = offer }
    })
    Object.keys(openTrades).forEach(offerId => {
      const offer = openTrades[offerId]
      if (offer.seller !== playerName) {
        const offerSent = offer.buyers.includes(playerName)
        const offerAvailable = (myOffer !== null && myOffer.buyers.includes(offer.seller))
        filteredTrades.push({ id: offerId, seller: offer.seller, quantity: offer.quantity, offerSent, offerAvailable })
      }
    })
    const targetSocket = io.sockets.connected[playerData[playerName].socketId]
    const { hand, addedToGame }  = playerData[playerName]
    targetSocket.emit("action", { type: "GAME_DATA", payload: { hand, playerName, openTrades: filteredTrades, myOffer } })
  })
}

function clearPendingOffers(name) {
  Object.keys(openTrades).forEach(offerId => {
    const offer = openTrades[offerId]
    if (offer.seller === name) {
      delete openTrades[offerId]
    }
    if (offer.buyers.includes(name)) {
      offer.buyers.splice(offer.buyers.indexOf(name), 1)
    }
  })
  delete playerData[name].pendingOffer
}

function setupGame() {
  otherData.gameStarted = true
  const connectedPlayers = getConnectedPlayers()
  let deck = []
  for (let suit = 1; suit < connectedPlayers.length+1; suit++) {
    for (let count = 0; count < 9; count++) {
      deck.push(defaultCommodities[suit])
    }
  }
  shuffleArray(deck)
  connectedPlayers.forEach(player => {
    const playerName = player.name
    clearPendingOffers(playerName)
    playerData[playerName].addedToGame = true
    playerData[playerName].hand = []
    for (let count = 0; count < 9; count++) {
      playerData[playerName].hand.push(deck.pop())
    }
    const targetSocket = io.sockets.connected[playerData[playerName].socketId]
    targetSocket.emit("action", { type: "GAME_START_SUCCESS", payload: { gameStarted: true, addedToGame: true } })
  })
}

function checkForFinalizeTrade(data) {
  const tradeA = openTrades[data.tradeId]
  let tradeB = null

  Object.keys(openTrades).forEach(tradeId => {
    if (openTrades[tradeId].seller == data.playerName) { tradeB = openTrades[tradeId] }
  })

  if ( tradeB && tradeA.buyers.includes(tradeB.seller) && tradeB.buyers.includes(tradeA.seller)) {
    finalizeTrade(tradeA, tradeB)
    return true
  } else { return false }
}

function finalizeTrade(tradeA, tradeB) {
  removeCards(tradeA)
  removeCards(tradeB)
  addCards(tradeA.seller, tradeB)
  addCards(tradeB.seller, tradeA)
  clearPendingOffers(tradeA.seller)
  clearPendingOffers(tradeB.seller)
}

function addCards(recipient, trade) {
  const { hand } = playerData[recipient]
  for (let i = 0; i < trade.quantity; i++){
    hand.push(trade.commodity)
  }
  const targetSocket = io.sockets.connected[playerData[recipient].socketId]
  targetSocket.emit("action", { type: "NOTIFICATION", payload: `You recieved ${trade.quantity} ${trade.commodity}`})
}

function removeCards(trade) {
  const { hand } = playerData[trade.seller]
  for (let i = 0; i < trade.quantity; i++) {
    const newIndex = hand.indexOf(trade.commodity)
    hand.splice(newIndex,1)
  }
}

io.on("connection", async (socket) => {
    socket.on("action", async (action) => {
        switch (action.type) {
          case "server/ADD_PLAYER":
            if (otherData.gameStarted) {
              socket.emit("action", { type: "JOIN_GAME_SUCCESS", payload: { gameStarted: true, loggedIn: true, addedToGame: false }})
              break
            }
            const newPlayerData = Object.assign({wins: 0}, playerData[action.payload.playerName], {socketId: socket.id, connected: true})
            playerData[action.payload.playerName] = newPlayerData
            let connectedPlayers = getConnectedPlayers()
            socket.emit("action", { type: "JOIN_GAME_SUCCESS", payload: {
              connectedPlayers,
              gameStarted: otherData.gameStarted,
              loggedIn: true,
              me: action.payload.playerName,
              addedToGame: playerData[action.payload.playerName].addedToGame
            }})
            socket.broadcast.emit("action", { type: "UPDATE_PLAYERS", payload: { connectedPlayers } })
            updateGameDataForAllPlayers()
            break
          case "server/START_GAME":
            setupGame()
            updateGameDataForAllPlayers()
            break
          case "server/MAKE_OFFER":
            const player = playerData[action.payload.seller]
            const newId = uniqid()
            player.pendingOffer = newId
            openTrades[newId] = action.payload
            updateGameDataForAllPlayers()
            break
          case "server/WITHDRAW_OFFER":
            clearPendingOffers(action.payload)
            updateGameDataForAllPlayers()
            break
          case "server/SELECT_TRADE":
            let { seller, buyers } = openTrades[action.payload.tradeId]
            buyers.push(action.payload.playerName)
            const targetSocket = io.sockets.connected[playerData[seller].socketId]
            if (!checkForFinalizeTrade(action.payload)) {
              targetSocket.emit("action", { type: "NOTIFICATION", payload: `${action.payload.playerName} wants to trade with you!`})
            }
            updateGameDataForAllPlayers()
            break
          case "server/DESELECT_TRADE":
            buyers = openTrades[action.payload.tradeId].buyers
            buyers.splice(buyers.indexOf(action.payload.playerName), 1)
            updateGameDataForAllPlayers()
            break
          case "server/WIN_GAME":
            if (otherData.gameStarted) {
              otherData.gameStarted = false
              playerData[action.payload].wins++
              connectedPlayers = getConnectedPlayers()
              io.sockets.emit("action", { type: "UPDATE_PLAYERS", payload: { connectedPlayers, gameStarted: otherData.gameStarted } })
              io.sockets.emit("action", { type: "NOTIFICATION", payload: `${action.payload} wins!`})
              updateGameDataForAllPlayers()
            }
            break
        }
    })

    socket.on("disconnect", async () => {
      Object.keys(playerData).forEach(name => {
        if (playerData[name].socketId == socket.id) {
          playerData[name].socketId = null
          playerData[name].connected = false
          clearPendingOffers(name)
        }
      })
      const connectedPlayers = getConnectedPlayers()
      socket.broadcast.emit("action", { type: "UPDATE_PLAYERS", payload: { connectedPlayers } })
      updateGameDataForAllPlayers()
    })
})
