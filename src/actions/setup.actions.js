export const addPlayerToGame = playerName => ({ type: "server/ADD_PLAYER", payload: playerName })
export const startGame = () => ({ type: "server/START_GAME" })
