import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { PersistGate } from "redux-persist/es/integration/react"
import configureStore from "./store"
import io from "socket.io-client"
import AppContainer from "./components/AppContainer"


require("babel-polyfill")

const url = LOCAL ? "ws://localhost:3000" : "ws://159.203.186.34:3000"

const socket = io.connect(url)
socket.emit({"hello":"hello"})
const { store, persistor } = configureStore(socket)

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={ persistor }>
      <AppContainer />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
)
