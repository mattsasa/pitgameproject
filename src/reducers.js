import setupReducer from "./reducers/setup.reducer"
import gameReducer from "./reducers/game.reducer"
import { persistCombineReducers } from "redux-persist"
import storage from "redux-persist/lib/storage"

const config = {
    key: "root",
    storage,
    whitelist: ["*"]
  }

const rootReducer = persistCombineReducers(config, { setupReducer, gameReducer })

export default rootReducer
