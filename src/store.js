import { applyMiddleware, compose, createStore } from "redux"
import thunk from "redux-thunk"
import rootReducer from "./reducers"
import createSocketIoMiddleware from "redux-socket.io"
import { persistStore } from "redux-persist"
import reduxLogger from "redux-logger"

export default function configureStore(socket) {
    let socketIoMiddleware = createSocketIoMiddleware(socket, "server/")

    const middleware = compose(
        applyMiddleware(socketIoMiddleware, thunk)
    )

    let store = createStore(rootReducer, undefined, middleware)
    store.subscribe(() => {
        // console.log("new client state", store.getState())
    })
    let persistor = persistStore(store)
    return { persistor, store }
}
