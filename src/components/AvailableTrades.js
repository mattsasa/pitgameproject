import React, { Component } from "react"
import { Col, Card, CardBody, CardTitle, Row } from 'reactstrap'
import { connect } from "react-redux"

class AvailableTrades extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <Col className="mx-auto" xs="10">
        <Card>
          <CardBody>
            <CardTitle>Available Trades</CardTitle>
            <Row>
              { this.props.displayAvailableTrades }
            </Row>
          </CardBody>
        </Card>
      </Col>

    )
  }
}
export default connect()(AvailableTrades)
