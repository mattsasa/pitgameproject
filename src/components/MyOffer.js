import React, { Component } from "react"
import { Col, Card, CardBody, CardTitle, Button } from 'reactstrap'
import { connect } from "react-redux"

class MyOffer extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <Col xs="2"  style={{ display: this.props.pendingOffer }} >
        <Card>
          <CardBody>
            <CardTitle>My Offer</CardTitle>
            { this.props.displayMyOffer }
            <Button size="sm" color="primary" onClick={ this.props.withdrawOffer } >Withdraw Offer</Button>
          </CardBody>
        </Card>
      </Col>
    )
  }
}
export default connect()(MyOffer)
