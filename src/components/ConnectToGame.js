import React, { Component } from "react"
import { Row, Col, Card, CardBody, CardTitle } from 'reactstrap'
import { connect } from "react-redux"
import { addPlayerToGame } from "../actions/setup.actions"

const mapDispatchToProps = dispatch => {
  return {
    addPlayerToGame: playerName => dispatch(addPlayerToGame(playerName))
  }
}

class ConnectToGame extends Component {
  constructor() {
    super()
    this.state = { playerName: "" }
    this.connectToGame = this.connectToGame.bind(this)
    this.handleChangePlayerName = this.handleChangePlayerName.bind(this)
  }

  connectToGame(event) {
    event.preventDefault()
    const { playerName } = this.state
    this.props.addPlayerToGame({ playerName })
  }

  handleChangePlayerName(event) {
    this.setState({ playerName: event.target.value })
  }

  render() {
    const { playerName } = this.state

    return (
        <Row>
          <Col className="mx-auto" sm="4">
            <Card>
              <CardBody>
                <CardTitle>Connect to Game</CardTitle>
                <form id="article-form" onSubmit={this.connectToGame}>
                  <input className="form-control" type="text" id="playerName" placeholder="Player Name" required
                    onChange={this.handleChangePlayerName}
                    value={playerName}
                  />
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
    )
  }
}
export default connect(null, mapDispatchToProps)(ConnectToGame)
