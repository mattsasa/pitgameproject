import React, { Component } from "react"
import { Col, Row, Card, CardBody, CardTitle, Button } from 'reactstrap'
import { connect } from "react-redux"

class PlayerHand extends Component {

  render() {
    return (
        <Col style={{ display: this.props.gameStarted ? "inline" : "none" }} xs="10">
          <Card>
            <CardBody>
              <CardTitle>Current Hand</CardTitle>
              <Row>
                { this.props.displayHand }
              </Row>
              <br></br>
              <Row>
                <Col>
                  <Button style={{ display: this.props.displayOfferButton }} size="sm" color="primary" onClick={this.props.makeOffer} >Make Offer</Button>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
    )
  }
}
export default connect()(PlayerHand)
