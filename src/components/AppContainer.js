import React, { Component } from "react"
import { Container, Navbar, NavbarBrand } from 'reactstrap'
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import { connect } from "react-redux"
import ConnectToGame from "./ConnectToGame"
import GameRoom from "./GameRoom"


const mapStateToProps = state => {
  return { setupState: state.setupReducer }
}

class AppContainer extends Component {
  constructor() {
    super()
  }

  render() {
    const { loggedIn, gameStarted, addedToGame } = this.props.setupState
    let displayGame = loggedIn ? "" : "none"
    let displayNotInGame = "none"
    if (gameStarted && loggedIn && !addedToGame) { displayNotInGame = ""; displayGame = "none"  }

    return (
      <div className="app">
          <ToastContainer/>
          <div id="loader">
              <div className="spinner"/>
          </div>
          <Container fluid={ true }>
            <Navbar color="light" light expand="md">
              <NavbarBrand >Pit Card Game</NavbarBrand>
            </Navbar>
            <br></br>
            <div style={{ display: loggedIn ? "none" : ""}}>
              <ConnectToGame/>
            </div>
            <div style={{ display: displayGame }}>
              <GameRoom/>
            </div>
            <div style={{ display: displayNotInGame}} >
              <h4>The game started without you loser</h4>
            </div>
          </Container>
      </div>
    )
  }
}
export default connect(mapStateToProps)(AppContainer)
