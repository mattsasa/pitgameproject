import React, { Component } from "react"
import { Col, Row, Card, Input, CardText, CardFooter, CardBody, CardImg, CardTitle, CardSubtitle, Button } from 'reactstrap'
import { connect } from "react-redux"
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import { startGame } from "../actions/setup.actions"
import { makeOffer, withdrawOffer, selectTrade, deselectTrade, winGame } from "../actions/game.actions"
import PlayerHand from "./PlayerHand"
import ConnectedPlayers from "./ConnectedPlayers"
import AvailableTrades from "./AvailableTrades"
import MyOffer from "./MyOffer"

import Wheat from '../foodSprites/wheat.png'
import Bananna from '../foodSprites/Bananna.png'
import Carrot from '../foodSprites/carrot.png'
import Cherries from '../foodSprites/cherries.png'
import Corn from '../foodSprites/corn.png'
import Grapes from '../foodSprites/grapes.png'
import GreenPear from '../foodSprites/greenPear.png'
import RedApple from '../foodSprites/redApple.png'
import Watermelon from '../foodSprites/Watermelon.png'






const mapStateToProps = state => {
  return { setupState: state.setupReducer, gameState: state.gameReducer }
}

const mapDispatchToProps = dispatch => {
  return {
    startGame: () => dispatch(startGame()),
    makeOffer: offer => dispatch(makeOffer(offer)),
    withdrawOffer: playerName => dispatch(withdrawOffer(playerName)),
    selectTrade: data => dispatch(selectTrade(data)),
    deselectTrade: data => dispatch(deselectTrade(data)),
    winGame: playerName => dispatch(winGame(playerName))
  }
}

class GameRoom extends Component {

    componentWillReceiveProps(nextProps) {
      const pendingOffer = nextProps.gameState.myOffer !== null
      this.setState({ pendingOffer })
    }

    constructor() {
      super()
      this.state = { playerName: "", selectedIndexes: [], pendingOffer: false }
      this.startGame = this.startGame.bind(this)
      this.makeOffer = this.makeOffer.bind(this)
      this.withdrawOffer = this.withdrawOffer.bind(this)
      this.selectCard = this.selectCard.bind(this)
      this.selectTrade = this.selectTrade.bind(this)
      this.winButton = this.winButton.bind(this)
    }

    startGame() {
      this.props.startGame()
    }

    winButton() {
      const { hand, playerName } = this.props.gameState
      const allEqual = hand.every( (val, i, arr) => val === arr[0] )
      allEqual ? this.props.winGame(playerName) : toast.error("You are a N00b: You must all the cards the same commodity!")
    }

    withdrawOffer() {
      this.props.withdrawOffer(this.props.gameState.playerName)
      this.setState({ pendingOffer: false })
    }

    makeOffer() {
      let selectedCards = []
      let indexes = []
      this.props.gameState.hand.forEach((commodity, index) => {
        if (this.state.selectedIndexes.includes(index)) {
          selectedCards.push(commodity)
          indexes.push(index)
        }
      })
      const allEqual = selectedCards.every( (val, i, arr) => val === arr[0] )
      if (allEqual && selectedCards.length > 0) {
        const offer = {
                        seller: this.props.gameState.playerName,
                        commodity: selectedCards[0],
                        quantity: selectedCards.length,
                        buyers: [],
                        indexes
                      }
        this.props.makeOffer(offer)
        this.setState({ pendingOffer: true, selectedIndexes: [] })
      } else {
        toast.error("You are a N00b: Your offer must be all the same commodity!")
      }
    }

    selectCard(event) {
      event.preventDefault()
      if (!this.state.pendingOffer) {
        const index = parseInt(event.currentTarget.getAttribute("value"))
        const { hand } = this.props.gameState
        const { selectedIndexes } = this.state
        if (selectedIndexes.includes(index)) {
          selectedIndexes.splice(selectedIndexes.indexOf(index), 1)
        } else { selectedIndexes.push(index) }
        this.setState({})
      }
    }

    selectTrade(event) {
      event.preventDefault()
      const { pendingOffer } = this.state
      const { myOffer, openTrades, playerName } = this.props.gameState
      const { deselectTrade, selectTrade } = this.props
      const tradeId = event.currentTarget.getAttribute("value")
      let targetOffer = null
      openTrades.forEach(offer => {
        if (offer.id == tradeId) { targetOffer = offer }
      })
      if (pendingOffer && myOffer.quantity == targetOffer.quantity) {
        const payload = { playerName, tradeId }
        targetOffer.offerSent ? deselectTrade(payload) : selectTrade(payload)
      } else {
        toast.error("You are a N00b: You can only select trades that are of the same quantity as your offer!")
      }
    }

    displayMyOffer() {
      const { myOffer } = this.props.gameState
      return myOffer ? <CardText>{myOffer.commodity}  x{myOffer.quantity}</CardText> : null
    }

    listPlayers() {
        const { connectedPlayers, me } = this.props.setupState
        let list = []
        if (connectedPlayers) {
          connectedPlayers.forEach(player => {
            const { name, wins } = player
            list.push(
              <p key={name}>{ name == me ? `Me: ${name} - ${wins} wins` : `${name} - ${wins} wins`}</p>
            )
          })
        }
        return list
    }

    commodityImg(commodity) {
      switch (commodity) {
        case "Wheat":
          return Wheat
        case "Bananna":
          return Bananna
        case "Carrot":
          return Carrot
        case "Cherries":
          return Cherries
        case "Corn":
          return Corn
        case "Grapes":
          return Grapes
        case "Green Pear":
          return GreenPear
        case "Red Apple":
          return RedApple
        case "Watermelon":
          return Watermelon
      }
    }

    displayHand() {
      const { selectedIndexes } = this.state
      const { myOffer, hand } = this.props.gameState
      let cards = []
      hand.forEach((commodity, index) => {
        let style = {width: "6.2rem", height: "9rem"}
        let theme = selectedIndexes.includes(index) ? "border-success text-success" : "border-dark text-dark"
        selectedIndexes.includes(index) ? style["borderWidth"] = "medium" :
        theme = myOffer && myOffer.indexes.includes(index) ? "border-muted text-muted" : theme
        cards.push(
          <Col xs="auto" style={{"paddingRight":0}} key={index} value={index} onClick={this.selectCard}>
            <Card style={style} className={theme}>
              <CardImg style={{ width: "3.6rem", height: "3.6rem", marginTop: "0.2rem" }} className="mx-auto" src={this.commodityImg(commodity)} />
              <CardBody className="text-center">
                <CardText className="small " >{commodity}</CardText>
              </CardBody>
           </Card>
          </Col>
        )
      })
      return cards
    }

    displayAvailableTrades() {
      const { openTrades } = this.props.gameState
      let cards = []
      openTrades.forEach((offer, index) => {
        let style = offer.offerAvailable ? "border-primary text-primary" : "border-dark text-dark"
        style = offer.offerSent ? "border-muted text-muted" : style
        const displayAccept = offer.offerAvailable ? "inline" : "none"
        const displaySent = offer.offerSent ? "inline" : "none"
        cards.push(
          <Col xs="auto" key={index} value={offer.id} onClick={this.selectTrade}>
            <Card className={"text-center " + style}>
              <CardBody>
                <CardText>{offer.quantity}</CardText>
              </CardBody>
              <CardFooter>
                <CardText>{offer.seller}</CardText>
              </CardFooter>
              <div style= {{display: displayAccept}}>
                <CardFooter>
                  <CardText>Finalize Trade</CardText>
                </CardFooter>
              </div>
              <div style= {{display: displaySent}}>
                <CardFooter>
                  <CardText>Offer Sent</CardText>
                </CardFooter>
              </div>
           </Card>
          </Col>
        )
      })
      return cards
    }

    render() {
        const { gameStarted } = this.props.setupState
        const { pendingOffer } = this.state
        const displayStartButton = gameStarted ? "none" : ""
        const displayGameStarted = gameStarted ? "" : "none"
        const displayOfferButton = !pendingOffer && gameStarted ? "" : "none"
        const displayMyOffer = pendingOffer ? "" : "none"

        return (
            <div>
              <Row>
                <PlayerHand gameStarted={ gameStarted } displayHand={ this.displayHand() }  makeOffer={ this.makeOffer } displayOfferButton={ displayOfferButton } />
                <ConnectedPlayers listPlayers={ this.listPlayers() } startGame={ this.startGame } displayStartButton={ displayStartButton } />
              </Row>
              <br></br>
              <Row style={{ display: displayGameStarted}}>
                <MyOffer displayMyOffer={ this.displayMyOffer() } pendingOffer={ displayMyOffer } withdrawOffer={ this.withdrawOffer } />
                <AvailableTrades displayAvailableTrades={ this.displayAvailableTrades() }/>
              </Row>
              <br/>
              <Row style={{ display: displayGameStarted}}>
                <Col className="mx-auto text-center" xs="6">
                  <Button size="lg" color="success" onClick={this.winButton} >Ring Bell</Button>
                </Col>
              </Row>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameRoom)
