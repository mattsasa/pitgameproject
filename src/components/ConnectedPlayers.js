import React, { Component } from "react"
import { Col, Card, CardBody, CardTitle, Button } from 'reactstrap'
import { connect } from "react-redux"

class ConnectedPlayers extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <Col className="ml-auto text-center" xs="2">
        <Card>
          <CardBody>
            <CardTitle>Connected Players</CardTitle>
            { this.props.listPlayers }
            <Button style={{ display: this.props.displayStartButton }} color="primary" onClick={this.props.startGame}>Start Game</Button>
          </CardBody>
        </Card>
      </Col>
    )
  }
}
export default connect()(ConnectedPlayers)
