import { toast } from "react-toastify"

const initialState = {
  loggedIn: false,
  gameStarted: false,
  me: "",
  connectedPlayers: [],
  addedToGame: false
}
const setupReducer = (state = initialState, action) => {
    const newState = Object.assign({}, state)
    switch (action.type) {
      case "JOIN_GAME_SUCCESS":
        toast.success("Successfully Connected")
        return Object.assign({}, state, action.payload)
      case "UPDATE_PLAYERS":
        return Object.assign({}, state, action.payload)
      case "GAME_START_SUCCESS":
        return Object.assign({}, state, action.payload)
      default:
        return state
    }
}
export default setupReducer
