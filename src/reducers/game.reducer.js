import { toast } from "react-toastify"

const initialState = {
  playerName: "",
  hand: [],
  openTrades: [],
  myOffer: null
}
const gameReducer = (state = initialState, action) => {
    switch (action.type) {
      case "GAME_DATA":
        return Object.assign({}, state, action.payload)
      case "NOTIFICATION":
        toast.info(action.payload)
        return state
      default:
        return state
    }
}
export default gameReducer
